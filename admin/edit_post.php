<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <?php 
 
  include('includes/head.php'); ?>
</head>



<body class="hold-transition skin-blue sidebar-mini">
<?php include('includes/admin_content.php'); ?>


<?php
if (empty($_GET['id'])) {
    redirect("posts.php");
}
$post = Posts::find_by_id($_GET['id']);

if (isset($_POST['update'])){
    $post->category_name = $_POST['category_name'];
  $post->post_name = $_POST['post_name'];
  $post->post_content = $_POST['post_content'];
  $post->post_date = $_POST['post_date'];
  $post->author = $_POST['author'];
  $post->set_file($_FILES['file_upload']);

  $post->save();
   if ($post->update()) {
       echo "post uploaded succesfully";

   } else {
       $message = join("<br>", $post->errors);
   }
    
}


?>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Edit User
                   
                </h1>
                <!--  <div class="col-md-6">
                    <img class="img-responsive" src="<?php /* echo $user->image_path_and_placehoder(); */ ?>" alt="">
                </div> -->
                <form action="" method="post" enctype="multipart/form-data">
                    <!-- <div class="col-md-6 ">
                        <div class="form-group">
                            <input type="file" name="user_image"  >
                        </div> -->
                        <div class="form-group">
                            <label for="category_name">Category Name</label>
                            <input type="text" name="category_name" class="form-control" value=" <?php echo $post->category_name ; ?>" >
                        </div>
                        <div class="form-group">
                                <label for="post_name">Post Name</label>
                                <input type="text" name="post_name" class="form-control" value=" <?php echo $post->post_name; ?>" >
                            </div>
                    <div class="form-group">
                            <label for="post_content">Post Content</label>
                            <textarea  name="post_content" class="form-control"  cols="30" rows="10" value=""> <?php echo $post->post_content; ?> </textarea>
                        </div>
                    <div class="form-group">
                            <label for="post_date">Post Date</label>
                            <input type="text" name="post_date" class="form-control" value=" <?php echo $post->post_date; ?>" >
                        </div>
                    <div class="form-group">
                            <label for="author">Author</label>
                            <input type="text" name="author" class="form-control" value=" <?php echo $post->author;  ?>" >
                        </div>
                        <div class="form-group">
                            <input type="file" name="file_upload" >
                        </div>
                        <div class="form-group">
                            <a class="btn btn-danger" href="delete_post.php">Delete</a>    
                            <input type="submit" class="btn btn-primary pull-right" name="update" value="Update" ></div>
                        
                        </div>



                    
                </form>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<?php include('includes/admin_content1.php'); ?>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>