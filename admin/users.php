<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <?php 
 
  include('includes/head.php');
  $users = Users::find_all();
  $posts = Posts::find_all();

  ?>
  <?php /* if(!$session->is_signed_in()) {redirect("login.php");} */ ?> 
</head>



<body class="hold-transition skin-blue sidebar-mini">
<?php include('includes/admin_content.php'); ?>

<h3>Users Page</h3>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Users
                </h1>
                <a href="add_user.php" class="btn btn-primary">Add User</a>
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>User Name</th>
                                <th>Mail</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>User Posts</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($users as $user) : ?>
                            <tr>
                                <td><?php echo $user->id; ?> </td>
                                <td><?php echo $user->user_uid ?>
                                    <div class="action_links">
                                        <a href="delete_user.php?id=<?php echo $user->id; ?>">Delete</a>
                                        <a href="edit_user.php?id=<?php echo $user->id; ?>">Edit</a>
                                    </div>
                                </td>
                                <td><?php echo $user->user_email; ?></td>
                                <td><?php echo $user->user_first; ?></td>
                                <td><?php echo $user->user_last; ?></td>
                                <td><?php foreach ($posts as $post) {if($user->id == $post->user_id){echo $post->id.", ".$post->post_name."<br>" ;} } ?></td>
                                <td></td>
                            </tr>
                            <?php endforeach ; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- ./wrapper -->
<?php include('includes/admin_content1.php'); ?>

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>