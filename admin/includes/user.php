<?php

class Users 
{
    public static $db_table = "users"; // abstracting table
    public static $db_table_fields = array('user_uid','user_pwd','user_first','user_last','user_email');
    public $id;
    public $user_uid;
    public $user_pwd;
    public $user_first;
    public $user_last;
    public $user_email;
  
    public $errors = array();
    public $upload_array = array(
        UPLOAD_ERR_OK          => "There is no error",
        UPLOAD_ERR_INI_SIZE    => "The uploaded file excedes the upload_max_filesize directort",
        UPLOAD_ERR_FORM_SIZE   => "The uploaded file excedes the MAX_FILE_SIZE directive",
        UPLOAD_ERR_PARTIAL     => "The uploaded file was only partially uploaded",
        UPLOAD_ERR_NO_FILE     => "No file was uploaded",
        UPLOAD_ERR_NO_TMP_DIR  => "Missing a temprory folder",
        UPLOAD_ERR_CANT_WRITE  => "Failed to write file to disk",
        UPLOAD_ERR_EXTENSION   => "A php extension stopped the file upload"  
    );


    // This is passing $_FILES['uploaded_file'] as an argument
    public function set_file($file){
        if (empty($file) || !$file || !is_array($file)) {
            $this->errors[] = "There are no file uploaded here";
            return false;
        } elseif ($file['error'] !=0) {
            $this->errors[] = $this->upload_array($file['error']);
            return false;
        }else {
            $this->user_image = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->type     = $file['type'];
            $this->size     = $file['size'];
        }
    } // End of the set_file method

    public function upload_photo(){
        if ($this->id) {
            $this->update();
        } else {
            if (!empty($this->errors)) {
                return false;
            }
            if (empty($this->user_image) || empty($this->tmp_path)) {
                $this->errors[] = "the file was not available";
                return false;
            }
            $target_path = SITE_ROOT . DS . 'admin' . DS . $this->upload_directory . DS . $this->user_image;

            if (file_exists($target_path)) {
                $this->errors[] = "The file {$this->user_image} already exists";
                return false;
            }
            if (move_uploaded_file($this->tmp_path, $target_path)) {
                    unset($this->tmp_path);
                    return true;
            } else {
                $this->errors[] = "The file directory probably does not have permissions";
            }
        }
    } // End of the save method

    public function image_path_and_placehoder(){
        return empty($this->user_image) ? $this->image_placeholder : $this->upload_directory.DS.$this->user_image; 
    }
   
    public static function verify_user($username, $password)
    {
        global $database;
        $username = $database->escape_string($username);
        $password = $database->escape_string($password);

        $sql = "select * from " . self::$db_table . " where ";
        $sql .= " user_uid = '{$username}' ";
        $sql .= "AND user_pwd = '{$password}' ";
        $sql .= " LIMIT 1";
        $the_result_array = self::find_by_query($sql);
        return !empty($the_result_array) ? array_shift($the_result_array) : false;
    }
  


/* protected static $db_table = "users"; // abstracting table */


 /* public static function find_all_users(){
    global $database;
    $result_set = $database->query("select * from users");
    return $result_set;
} */

public static function find_all()
{
    // self -->> static it is lata static binding
    return static::find_by_query("select * from " . static::$db_table . " ");
} // End of the find_all_users method

public static function find_by_id($id)
{
    global $database;
    // $dd = $id;
    $the_result_array = static::find_by_query("select * from " . static::$db_table . " where id=$id limit 1");
    /* $found_user = mysqli_fetch_array($result_set);
    return $found_user; */
    return !empty($the_result_array) ? array_shift($the_result_array) : false;
} // End of the find_by_id method

 // for repatation
 public static function find_by_query($sql)
 {
     global $database;
     $result_set = $database->query($sql);
     $the_object_array = array();
     while ($row = mysqli_fetch_array($result_set)) {
         $the_object_array[] = static::instantation($row);
     }
     return $the_object_array;
 } // End of the find_by_query method

 /* public static function instantation($found_user)
{
    $the_object = new self;
   
    $the_object->id = $found_user['id'];
    $the_object->username = $found_user['username'];
    $the_object->password = $found_user['password'];
    $the_object->first_name = $found_user['first_name'];
    $the_object->last_name = $found_user['last_name'];

    return $the_object;
} */

// Same thing but better version
public static function instantation($the_record)
{   
    // self -->> $calling_class static binding
    $calling_class = get_called_class();
    $the_object = new $calling_class;

    foreach ($the_record as $the_attribute => $value) {
        if ($the_object->has_the_attribute($the_attribute)) {
            $the_object->$the_attribute = $value;
        }
    }
    return $the_object;
}
private function has_the_attribute($the_attribure)
{
    $object_properties = get_object_vars($this);
    return array_key_exists($the_attribure, $object_properties);
}

protected function properties(){ // for abstracting to properties
    $properties = array();

    foreach (static::$db_table_fields as $db_field) {
        if (property_exists($this, $db_field)) {
            $properties[$db_field] = $this->$db_field;
        }
    }
    return $properties;
}

protected function clean_properties(){
    global $database;

    $clean_properties = array();
    foreach ($this->properties() as $key => $value) {
        $clean_properties[$key] = $database->escape_string($value);
    }
    return $clean_properties;
}

public function save(){
    return isset($this->id) ? $this->update() : $this->create();
}

public function create()
{
    global $database;


   /*  $sql = "INSERT INTO " . self::$db_table . " (username, password, first_name, last_name)";
    $sql .= "VALUES ('";
    $sql .= $database->escape_string($this->username) . "', '";
    $sql .= $database->escape_string($this->password) . "', '";
    $sql .= $database->escape_string($this->first_name) . "', '";
    $sql .= $database->escape_string($this->last_name) . "')"; */

    // abstracted version 
    $properties = $this->clean_properties();
    
    $sql = "INSERT INTO " . static::$db_table . "(" . implode(",", array_keys($properties)) . ")";
    $sql .= "VALUES ('". implode("','", array_values($properties)) ."')";

    if ($database->query($sql)) {
        $this->id = $database->the_insert_id();
        return true;
    } else {
        return false;
    }
} // Create method

public function update()
{
    global $database;

   /*  $sql = "UPDATE " . self::$db_table . " SET";
    $sql .= "user_uid= '" . $database->escape_string($this->user_uid) . "', '";
    $sql .= "user_pwd= '" . $database->escape_string($this->user_pwd) . "', '";
    $sql .= "user_first= '" . $database->escape_string($this->user_first) . "', '";
    $sql .= "user_last= '" . $database->escape_string($this->last_name) . "' ";
    $sql .= " WHERE id= " . $database->escape_string($this->id); */

    // abstracted version
    $properties = $this->clean_properties();
    $properties_pairs = array();
    foreach ($properties as $key => $value) {
        $properties_pairs[] = "{$key}='{$value}'";
    }
    $sql = "UPDATE " . static::$db_table . " SET ";
    $sql .= implode(", ", $properties_pairs);
    $sql .= " WHERE id= " . $database->escape_string($this->id);

    $database->query($sql);

    return (mysqli_affected_rows($database->connection) == 1) ? true : false;
} // Update method

public function delete()
{
    global $database;

    $sql = "DELETE FROM " . static::$db_table . " ";
    $sql .= "WHERE id=" . $database->escape_string($this->id);
    $sql .= " LIMIT 1";
    
    $database->query($sql);
    return (mysqli_affected_rows($database->connection) == 1) ? true : false;
} // Delete method

    
} // End of the class
