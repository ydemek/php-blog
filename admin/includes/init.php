<?php
defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
define('SITE_ROOT', DS . 'C:' . DS . 'xampp' . DS . 'htdocs' . DS . 'callie');
defined('INCLUDES_PATH') ? null : define('INCLUDES_PATH', SITE_ROOT.DS.'admin'.DS.'includes');  

require_once("session.php");
require_once("functions.php");
require_once("database.php");
require_once("user.php");
require_once("post.php");
require_once("db_object.php");
?>