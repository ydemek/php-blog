<?php

class Posts extends Users{

    public static $db_table = "posts"; 
    public static $db_table_fields = array('user_id','category_name','post_name','post_content','post_date','author','filename','type','size');
    public $post_id;
    public $user_id;
    public $category_name;
    public $post_name;
    public $post_content;
    public $post_date;
    public $author;
    public $filename;
    public $type;
    public $size;

    public $tmp_path;
    public $upload_directory = "posts";
    public $errors = array();
    public $upload_array = array(
        UPLOAD_ERR_OK          => "There is no error",
        UPLOAD_ERR_INI_SIZE    => "The uploaded file excedes the upload_max_filesize directort",
        UPLOAD_ERR_FORM_SIZE   => "The uploaded file excedes the MAX_FILE_SIZE directive",
        UPLOAD_ERR_PARTIAL     => "The uploaded file was only partially uploaded",
        UPLOAD_ERR_NO_FILE     => "No file was uploaded",
        UPLOAD_ERR_NO_TMP_DIR  => "Missing a temprory folder",
        UPLOAD_ERR_CANT_WRITE  => "Failed to write file to disk",
        UPLOAD_ERR_EXTENSION   => "A php extension stopped the file upload"  
    );
    function upload_errors_array($er){
        return $this->upload_array[$er];
    }
    // This is passing $_FILES['uploaded_file'] as an argument
    public function set_file($file){
        if (empty($file) || !$file || !is_array($file)) {
            $this->errors[] = "There are no file uploaded here";
            return false;
        } elseif ($file['error'] !=0 && $file['error'] !=4 ) {
            $this->errors[] = $this->upload_errors_array($file['error']);
            var_dump($this->errors);
            return false;
        }else {
            $this->filename = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->type     = $file['type'];
            $this->size     = $file['size'];
            
        }
    } // End of the set_file method

    public function picture_path(){
        return $this->upload_directory.DS.$this->filename;
    }

    public function save(){
        if ($this->post_id) {
            $this->update();
        } else {
            if (!empty($this->errors)) {
                return false;
            }
            if (empty($this->filename) || empty($this->tmp_path)) {
                $this->errors[] = "the file was not available";
                return false;
            }
            $target_path = SITE_ROOT . DS . 'admin' . DS . $this->upload_directory . DS . $this->filename;

            if (file_exists($target_path)) {
                $this->errors[] = "The file {$this->filename} already exists";
                return false;
            }
            if (move_uploaded_file($this->tmp_path, $target_path)) {
                if ($this->create()) {
                   /*  echo "creating"; */
                    unset($this->tmp_path);
                    return true;
                }
            } else {
               /*  echo "tmp_path " . $this->tmp_path;
                echo "<br>target_path " .  $target_path;
                echo "create " . $this->create(); */
                $this->errors[] = "The file directory probably does not have permissions";
            }
        }
    } // End of the save method

    public function delete_photo(){
        if ($this->delete()) {
            $target_path = SITE_ROOT.DS. 'admin' . DS . $this->picture_path();
            return unlink($target_path) ? true : false; 
        } else {
            return false;
        }
    }

} // End of the class


?>