<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>
    <?php

    include('includes/head.php'); ?>
    <?php
    $posts = Posts::find_all();
    ?>
</head>



<body class="hold-transition skin-blue sidebar-mini">
    <?php include('includes/admin_content.php'); ?>

    <h3>Uploads Page</h3>
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Posts
                    </h1>
                    <a href="add_post.php" class="btn btn-primary">Add Post</a>
                    <div class="col-md-12">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Posts</th>
                                    <th>Category</th>
                                    <th>Name</th>
                                    <th>Content</th>
                                    <th>author</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($posts as $post) : ?>
                                    <tr>
                                        <td><img class="admin-post-thumbnail" src="<?php echo $post->picture_path(); ?>" alt="">
                                            <div class="pictures_link">
                                                <a href="delete_post.php?id=<?php echo $post->id; ?>">Delete</a>
                                                <a href="edit_post.php?id=<?php echo $post->id; ?>">Edit</a>
                                                <a href="view_post.php?id=<?php echo $post->id; ?>">View</a>
                                            </div>
                                        </td>
                                        <td><?php echo $post->category_name ?></td>
                                        <td><?php echo $post->post_name; ?></td>
                                        <td><?php echo $post->post_content; ?></td>
                                        <td><?php echo $post->author; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
    <?php include('includes/admin_content1.php'); ?>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>

</html>