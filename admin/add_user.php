<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <?php 
 
  include('includes/head.php'); ?>
</head>



<body class="hold-transition skin-blue sidebar-mini">
<?php include('includes/admin_content.php'); ?>

<h3>Uploads Page</h3>
<?php  
 /* if (!$session->is_signed_in()) {
    redirect("login.php");
}  */
$user = new Users();
if (isset($_POST['create'])){
    $user->user_uid = $_POST['username'];
    $user->user_pwd = $_POST['password'];
    $user->user_first = $_POST['first_name'];
    $user->user_last = $_POST['last_name'];
    $user->user_email = $_POST['user_email'];
   

   /*  $user->set_file($_FILES['user_image']); */

    $user->create();
}
?>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Add User
                   
                </h1>
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="col-md-6 col-md-offset-3">
                        <!-- <div class="form-group">
                            <input type="file" name="user_image"  >
                        </div> -->
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" name="username" class="form-control"  >
                        </div>
                        <div class="form-group">
                            <label for="username">Mail</label>
                            <input type="emai" name="user_email" class="form-control"  >
                        </div>
                        <div class="form-group">
                            <label for="first name">First Name</label>
                            <input type="text" name="first_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="last name">Last Name</label>
                            <input type="text" name="last_name" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="text" name="password" class="form-control" >
                        </div>
                        <div class="form-group"><input type="submit" class="btn btn-primary pull-right" name="create" ></div>
                        
                    </div>



                    
                </form>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<?php include('includes/admin_content1.php'); ?>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>